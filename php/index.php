<?php

define('CONFIG_FILE', './config.ini');
$ini = parse_ini_file(CONFIG_FILE);
define('LOG_FILE', $ini['log_file']);

function logMsg($message) {
    $fp = fopen(LOG_FILE, 'a') ;
    fwrite($fp, "$message\n") ;
    fclose($fp) ;
}

function generateHeaderResponse() {
    header('content-type:application/json');
}

function generateVmBoxBodyResponse($vmBoxId) {
    logMsg("Generate Vmbox Body Response");
    $template = '{"module":"voicemail","data":{"id":"%s"}}';
    return sprintf($template, $vmBoxId);
}

function generateDeviceBodyResponse($deviceId) {
    logMsg("Generate Device Body Response");
    $template = '{"module":"device","data":{"id":"%s"}}';
    return sprintf($template, $deviceId);
}

/**
 * Minimum requirements
 *   Account-Id
 *   To
 *   From
 *   Api-Verson (not needed, but represents issue if absent)
 *
 * @param $parameters array
 * @return bool
 */
function validateResponse($parameters) {
    if (!isset($parameters['Account-ID']) ||
        !isset($parameters['To'])         ||
        !isset($parameters['From'])       ||
        !isset($parameters['Api-Version'])
    ) {
        logMsg("Required parameter missing from this request ..  stopping");
        return $parsed = false ;
    }
    foreach($parameters as $parameter => $value) {
        logMsg("Parsing this request .. $parameter => $value ");
    }
    return true;
}

/**
 * Matches a AccountID and the number being called to known details in order to return data (in this case
 *      a name associated with the account and number)
 * @param $parameters array
 * @return false|mixed
 */
function generateResponse($parameters) {
    global $ini;

    $accountId = $parameters['Account-ID'];
    $numberTo = $parameters['To'];
    $knownAccountNumbers = $ini['knows_accounts_numbers'];
    $inOfficeStatus = $ini['number_customer_in_office_status'];
    $numberVoicemailId = $ini['number_voicemail_id'];
    $numberDeviceId = $ini['number_device_id'];

    $returnCallflow = "";
    // Check the account associated with the number is known
    if (array_key_exists($accountId, $knownAccountNumbers)) {
        logMsg("AccountId lookup successful");
        // Check if the number is one we track the out of office status for
        if (array_key_exists($numberTo, $inOfficeStatus)) {
            logMsg("Number lookup successful");
            if (!$inOfficeStatus[$numberTo]) {
                // If customer out of office send to voicemail
                $returnCallflow = generateVmBoxBodyResponse($numberVoicemailId[$numberTo]);
            } else {
                // If customer in office direct to their device
                $returnCallflow = generateDeviceBodyResponse($numberDeviceId[$numberTo]);
            }
        } else {
            logMsg("Number lookup failed");
        }
    } else {
        logMsg("AccountId lookup failed");
    }
    return $returnCallflow;
}

function main() {
    $startTime = round(microtime(true) * 1000);

    logMsg("New Pivot call .. ". date("Y-M-d H:i:s"));
    logMsg("Request data from Kazoo, " . json_encode($_REQUEST));
    $body = null;
    if (validateResponse($_REQUEST)) {
        generateHeaderResponse();
        $body = generateResponse($_REQUEST);
    }
    echo $body;
    $endTime = round(microtime(true) * 1000) - $startTime;
    logMsg("Processing Complete, Time: " . $endTime);
}

main() ;

//----------------------------------------------------------------------------
