# Php 

## Version requirements

* Php 5.6.9

## Some production setup information and tutorials (on Linux)

If you are running your endpoint on a traditional non dockerized webserver you'll likely need to add a custom httpd config file

* [pivot_example.conf](pivot_example.conf)

Links to help you get setup

* [5 ways to deploy PHP applications](https://buddy.works/guides/5-ways-to-deploy-php-applications)
* [How to install Apache, PHP 7.3 and MySQL on CentOS 7.6](https://www.howtoforge.com/tutorial/centos-lamp-server-apache-mysql-php/)

## Local testing

You can start a web server to test the program by running `python3 flask_app.py` this will run the app directly.  
The included wsgi file is only needed if you are running the end point on a server using the wsgi_module

### Install php.

[How to Install PHP 5.6, PHP 7.3 on Ubuntu 18.04 & 16.04 using PPA](https://tecadmin.net/install-php5-on-ubuntu/)


```
bash
php -S localhost:3000
```

Will start on localhost port 3000, however this will not be able to be called from Kazoo. A useful tool
for testing locally is [Insomnia](https://insomnia.rest/)