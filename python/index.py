from flask import Flask, request
import json, os, datetime, sys, time

app = Flask(__name__)
app.config["DEBUG"] = True

dirname = os.path.dirname(__file__)
with open(os.path.join(dirname, "config.json")) as json_data_file:
    config = json.load(json_data_file)

def logMsg(msg):
    global config
    global dirname
    file_object = open(os.path.join(dirname, config['parameters']['log_file']), 'a+')
    file_object.write(msg)
    file_object.close()

# Header inserted by python automatically at data return

def generateVmBoxBodyResponse(vmBoxId):
    logMsg("\nGenerate Vmbox Body Response")
    template = '{"module":"voicemail","data":{"id":"' + vmBoxId + '"}}'
    return template

def generateDeviceBodyResponse(deviceId):
    logMsg("\nGenerate Device Body Response")
    template = '{"module":"device","data":{"id":"' + deviceId + '"}}'
    return template

def validateResponse(parameters):
    """ Minimum requirements
         Account-Id
         To
         From
         Api-Verson (not needed, but represents issue if absent)
    """
    if (
        not("Account-ID" in parameters)
        or not("To" in parameters)
        or not("From" in parameters)
        or not("Api-Version" in parameters)
    ):
        logMsg("\nRequired parameter missing from this request ..  stopping")
        return False

    for key in parameters:
        logMsg("\nParsing this request .. " + key + "=>" + parameters[key])
    return True

def generateResponse(parameters):
    """ Matches a AccountID and the number being called to known details in order to return data (in this case
     a name associated with the account and number)
    """
    global config

    accountId = parameters['Account-ID']
    numberTo = parameters['To']
    knownAccountNumbers = config['parameters']['knows_accounts_numbers']
    inOfficeStatus = config['parameters']['number_customer_in_office_status']
    numberVoicemailId = config['parameters']['number_voicemail_id']
    numberDeviceId = config['parameters']['number_device_id']

    returnCallflow = ""
    # Check the account associated with the number is known
    if (accountId in knownAccountNumbers):
        logMsg("\nAccountId lookup successful")
        # Check if the number is one we track the out of office status for
        if (numberTo in inOfficeStatus):
            logMsg("\nNumber lookup successful")
            if (not(inOfficeStatus[numberTo])):
                # If customer out of office send to voicemail
                returnCallflow = generateVmBoxBodyResponse(numberVoicemailId[numberTo])
            else:
                # If customer in office direct to their device
                returnCallflow = generateDeviceBodyResponse(numberDeviceId[numberTo])
        else:
            logMsg("\nNumber lookup failed")
    else:
        logMsg("\nAccountId lookup failed")
    return returnCallflow

@app.route("/", methods=['POST'])
def pivot():
    startTime = millis = int(round(time.time() * 1000))

    logMsg("\nNew Pivot call .. " + datetime.datetime.now().strftime("%Y-%b-%d %H:%M:%S"))
    logMsg("\nRequest data from Kazoo, " + json.dumps(request.values.to_dict()))
    body = null
    if (validateResponse(request.values)):
        body = generateResponse(request.values)

    endTime = int(round(time.time() * 1000)) - startTime
    logMsg("Processing Complete, Time: " + str(endTime))
    return body

if __name__ == "__main__":
    app.run()