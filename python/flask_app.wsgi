import logging
import sys
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, '/opt/pivot_test')
from index import app as application
application.secret_key = 'anything you wish'